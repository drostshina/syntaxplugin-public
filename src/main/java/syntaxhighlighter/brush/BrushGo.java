package syntaxhighlighter.brush;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Go brush
 * @author Daria Rostshina <daria.rostshina@corp.mail.ru>
 */
public class BrushGo extends Brush {
    public BrushGo() {
        super();

        String datatypes = "bool byte complex64 complex128 error float32 float64 "
                + "int int8 int16 int32 int64 rune string "
                + "uint uint8 uint16 uint32 uint64 uintptr";
        String keywords = "break case chan const continue default defer else "
                + "fallthrough for func go goto if import interface map "
                + "package range return select struct switch type var";
        String constants =  "true false nil iota";
        String functions = "append cap close complex copy delete imag len "
                + "make new panic print println real recover";

        List<RegExpRule> _regExpRuleList = new ArrayList<RegExpRule>();
        _regExpRuleList.add(new RegExpRule(RegExpRule.singleLineCComments, "comments")); // one line comments
        _regExpRuleList.add(new RegExpRule(RegExpRule.multiLineCComments, "comments")); // multiline comments
        _regExpRuleList.add(new RegExpRule(RegExpRule.doubleQuotedString, "string")); // strings
        _regExpRuleList.add(new RegExpRule(RegExpRule.singleQuotedString, "string")); // strings
        _regExpRuleList.add(new RegExpRule(getKeywords(datatypes), Pattern.MULTILINE, "keyword")); // keyword
        _regExpRuleList.add(new RegExpRule("\\b[\\d\\.]+\\b", "value")); // numbers
        _regExpRuleList.add(new RegExpRule("\\b0[0-7]+\\b", "value")); // octal numbers
        _regExpRuleList.add(new RegExpRule("\\b0(x|X)[a-fA-F0-9]+\\b", "value")); // hex numbers
        _regExpRuleList.add(new RegExpRule(getKeywords(functions), Pattern.MULTILINE, "functions")); // functions
        _regExpRuleList.add(new RegExpRule(getKeywords(constants), Pattern.MULTILINE, "constants")); // constants
        _regExpRuleList.add(new RegExpRule(getKeywords(keywords), Pattern.MULTILINE, "keyword")); // keyword

        setRegExpRuleList(_regExpRuleList);

        setCommonFileExtensionList(Arrays.asList("go"));
    }
}
